package com.voidkey.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Ejercicio {

	private static boolean estadoServicio = true;
	
	public static void main(String[] args) {
		
		ObjectInputStream in;
		ObjectOutputStream out;
		Socket socket = null;
		try (ServerSocket server = new ServerSocket(5555)) {
			System.out.println("Escuchando... por el puerto 5555");
			
			while (estadoServicio) {
				socket = server.accept();
				in = new ObjectInputStream(socket.getInputStream());
				out = new ObjectOutputStream(socket.getOutputStream());
				//Validamos el mensaje
				String mensaje = (String) in.readObject();
				
				if("apagar".equalsIgnoreCase(mensaje)) {
					System.out.println("Apagando Servidor");
					out.writeObject("El servicio se apagara...");
					apagarServidor();
				}else
					out.writeObject("Tu mensaje fue obtenido exitosamente.");
				
				System.out.println("El mensaje recibido es [["+mensaje+"]]");
			}
			
		}catch(IOException | ClassNotFoundException e)
		{
			System.out.println("Error al iniciar el servicio "+ e);
		}

	}
	
	private static void apagarServidor()
	{
		estadoServicio = false;
	}

}
